﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.Attributes;

namespace BanSQL
{

    [PluginDetails(author = "Flo - Fan", configPrefix = "bsql", description = "Ban and Warn gestion with MySQL DataBase.", id = "flo.bsql", langFile = "bansql", name = "BanSQL", SmodMajor = 3, SmodMinor = 4, SmodRevision = 0, version = "0.0.1")]

    public class Main : Plugin
    {
        public override void OnDisable()
        {
            this.Info("The plugin has been disabled.");
        }

        public override void OnEnable()
        {
            this.Info("The plugin has been enabled.");
        }

        public override void Register()
        {
            this.AddEventHandlers(new EventHandlers.OnCommandEvent(this));
            this.AddEventHandlers(new EventHandlers.OnWaitingForPlayerEvent(this));
            this.AddEventHandlers(new EventHandlers.BanEvents(this));

            // SQL Config

            AddConfig(new Smod2.Config.ConfigSetting("bsql_sql_ipaddress", "127.0.0.1", true, "Enter the IP Address of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_sql_username", "root", true, "Enter the username of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_sql_password", string.Empty, true, "Enter the password of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_sql_database", "scpsl", true, "Enter the name of the database"));

            AddConfig(new Smod2.Config.ConfigSetting("bsql_sql_bantable", "ban", true, "Enter the name of the table where the ban data will be stored"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_sql_warntable", "warn", true, "Enter the name of the table where the warn data will be stored"));

            AddConfig(new Smod2.Config.ConfigSetting("bsql_warn_enable", true, true, "Warn module enabled or not"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_warn_activedurate", "1M", true, "Expiration time for a warn"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_warn_autoban", 5, true, "Autoban player when he breaks the limit fix by this config."));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_warn_autoban_duration", "50y", true, "Autoban duration."));


            AddConfig(new Smod2.Config.ConfigSetting("bsql_ban_enable", true, true, "Ban module enabled or not"));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_ban_usepluginsystem", false, true, "Disable SCP SL's ban system and replace it by the plugin ban system."));
            AddConfig(new Smod2.Config.ConfigSetting("bsql_ban_ignorerole", new string[] { "owner" }, true, "When someone try to ban a player and he has got an \"ignore role\", he can't ban he."));

            // Commands

            AddCommands(new string[] { "warn", "wa" }, new Commands.Warn(this));
            AddCommands(new string[] { "sw_unban" }, new Commands.Ban(this));

            // Translation

            this.AddTranslation(new Smod2.Lang.LangSetting("general.exception", "Exception detected : [exception]", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("general.bddoff", "The database is OFF.", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("general.yes", "Yes", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("general.no", "No", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("general.playernotfound", "Error, player not found.", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("general.invalidvalue", "Error, Invalide value [error]", "bansql"));

            this.AddTranslation(new Smod2.Lang.LangSetting("ban.defaultReason", "No reason was given", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("ban.banMessage", "You have been banned\\nReason : [reason]\\nAdmin : [admin]\\nUnban Date : [unbanDate]", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("ban.ipbanMessage", "You're IP Address [ip] is banned\\nReason : [reason]\\nAdmin : [admin]\\nUnban Date : [unbanDate]", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("ban.disabled", "Ban module disabled", "bansql"));
            
            this.AddTranslation(new Smod2.Lang.LangSetting("warn.autoban.banMessage", "You've been banned because you've reached\\nthe maximum warn cap", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("warn.disabled", "Warn module disabled", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("warn.broadcast.playerWarned", "[player] has been warned by [admin]\\nfor the reason\\n[warn]", "bansql"));

            this.AddTranslation(new Smod2.Lang.LangSetting("command.personnalWarn.warn", "You've got [number] warns ([nbactive] active) :\\n[warn]", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("command.personnalWarn.nowarn", "You haven't got any warn.", "bansql"));

            this.AddTranslation(new Smod2.Lang.LangSetting("command.playerWarned", "[player] has been successfully warned for [reason]", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("command.playerBanned", "[player] has been successfully banned for [reason] during [time].", "bansql"));

            this.AddTranslation(new Smod2.Lang.LangSetting("command.getWarn", "Warn for player [player] :\\n[warns]", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("command.getWarn.noWarn", "The player has no warn.", "bansql"));

            this.AddTranslation(new Smod2.Lang.LangSetting("command.success", "Success", "bansql"));
            this.AddTranslation(new Smod2.Lang.LangSetting("command.error", "Error with the command [command] : [exception]", "bansql"));

        }
    }
}
