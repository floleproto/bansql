﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanSQL.Lib
{
    class StringToDateTime
    {
        public static TimeSpan ParseStringDuration(string duration)
        {
            if (!int.TryParse(new string(duration.Where(Char.IsDigit).ToArray()), out int amount))
            {
                return TimeSpan.MinValue;
            }

            char unit = duration.Where(Char.IsLetter).ToArray()[0];
            TimeSpan time = new TimeSpan();

            switch (unit)
            {
                case 's':
                    time = new TimeSpan(0,0,amount);
                    break;

                case 'm':
                    time = new TimeSpan(0, amount, 0);
                    break;

                case 'h':
                    time = new TimeSpan(amount, 0,0);
                    break;

                case 'd':
                    time = new TimeSpan(amount, 0, 0, 0);
                    break;

                case 'w':
                    time = new TimeSpan(amount * 7, 0, 0, 0);
                    break;

                case 'M':
                    time = new TimeSpan(amount * 30, 0, 0, 0);
                    break;

                case 'y':
                    time = new TimeSpan(amount * 365, 0, 0, 0);
                    break;

                default:
                    time = new TimeSpan(amount * 30, 0, 0, 0);
                    break;
            }

            return time;
        }
    }
}
