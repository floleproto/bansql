﻿using System;
using BanSQL;
using BanSQL.Lib;
using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;

namespace EventHandlers
{
    internal class BanEvents : IEventHandlerBan, IEventHandlerPlayerJoin
    {
        private Main main;

        public BanEvents(Main main)
        {
            this.main = main;
        }

        public void OnBan(BanEvent ev)
        {
            if(ConfigManager.Manager.Config.GetBoolValue("bsql_ban_enable", true))
            {
                try
                {
                    if (ev.AllowBan == true || ev.Duration > 0)
                    {
                        Ban banClass = new Ban(main);

                        string reason = LangManager.Manager.GetTranslation("ban.defaultReason").Replace("\\n", "\n");

                        if (ev.Reason.Length > 1) { reason = ev.Reason; };

                        banClass.AddBan(ev.Player.SteamId, reason, ev.Admin.Name, ev.Duration.ToString(), ev.Player.IpAddress.Trim("::ffff:".ToCharArray()));

                        if (ConfigManager.Manager.Config.GetBoolValue("bsql_ban_usepluginsystem", false))
                        {
                            ev.AllowBan = false;

                            string msg = LangManager.Manager.GetTranslation("ban.banMessage").Replace("\\n", "\n");
                            msg = msg.Replace("\\n", "\n");
                            msg = msg.Replace("[reason]", reason);
                            msg = msg.Replace("[admin]", ev.Admin.Name);
                            msg = msg.Replace("[unbanDate]", DateTime.Now.AddMinutes(ev.Duration).ToString("yyyy/MM/dd HH:mm:ss"));

                            ev.Player.Ban(0, "\n" + msg);
                        }
                    }
                }
                catch (System.NullReferenceException)
                {

                }
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (main.GetConfigBool("bsql_ban_usepluginsystem"))
            {
                Ban banClass = new Ban(main);
                if (!banClass.CheckBDDisOn())
                {
                    ev.Player.Ban(0, main.GetTranslation("general.bddoff"));
                }
            }
            if (ConfigManager.Manager.Config.GetBoolValue("bsql_ban_enable", true))
            {
                Ban banClass = new Ban(main);
                string ipaddress = ev.Player.IpAddress.Trim("::ffff:".ToCharArray());

                if (banClass.SteamIDIsBanned(ev.Player.SteamId) != null)
                {
                    BanObject ban = banClass.SteamIDIsBanned(ev.Player.SteamId);
                    string msg = LangManager.Manager.GetTranslation("ban.banMessage").Replace("\\n", "\n");
                    msg = msg.Replace("\\n", "\n");
                    msg = msg.Replace("[reason]", ban.reason);
                    msg = msg.Replace("[admin]", ban.admin);
                    msg = msg.Replace("[unbanDate]", ban.unbanTime.ToString("yyyy/MM/dd HH:mm:ss"));

                    ev.Player.Ban(0, "\n" + msg);
                }

                if (banClass.IPIsBanned(ipaddress) != null)
                {
                    BanObject ban = banClass.IPIsBanned(ipaddress);
                    string msg = LangManager.Manager.GetTranslation("ban.ipbanMessage").Replace("\\n", "\n");
                    msg = msg.Replace("\\n", "\n");
                    msg = msg.Replace("[ip]", ipaddress);
                    msg = msg.Replace("[reason]", ban.reason);
                    msg = msg.Replace("[admin]", ban.admin);
                    msg = msg.Replace("[unbanDate]", ban.unbanTime.ToString("yyyy/MM/dd HH:mm:ss"));

                    ev.Player.Ban(0, "\n" + msg);
                }
            }
        }
    }
}