# BanSQL

## Description

A plugin to manage ban and warn by using a MySQL Database.

## Configurations

### MySQL Database Configurations

|Config Key | Type Value | Default Value | Description|
|-----------|------------|---------------|------------|
|`bsql_sql_ipaddress`| String | `127.0.0.1` | Enter the IP Address of the database |
|`bsql_sql_username`| String | `root` | Enter the username of the database |
|`bsql_sql_password`| String | Empty | Enter the password of the database |
|`bsql_sql_name`| String | `scpsl` | Enter the name of the database |

### Ban Configurations

|Config Key | Type Value | Default Value | Description|
|-----------|------------|---------------|------------|
|`bsql_sql_warntable`| String | `warn` | Enter the name of the table where the warn data will be stored |
|`bsql_ban_enable`| Boolean | `true` | Enable the ban module |
|`bsql_ban_usepluginsystem`| String | `false` | Disable SCP SL's ban system and replace it by the plugin ban system. |
|`bsql_ban_ignorerole`| List String | `owner` |  |
|`bsql_sql_name`| String | `scpsl` | When someone try to ban a player and he has got an \"ignore role\", he can't ban he. |

### Warn Configurations

|Config Key | Type Value | Default Value | Description|
|-----------|------------|---------------|------------|
|`bsql_sql_bantable`| String | `ban` | Enter the name of the table where the ban data will be stored |
|`bsql_ban_enable`| Boolean | `true` | Enable the ban module |
|`bsql_warn_enable`| Boolean | `true` | Warn module enabled or not |
|`bsql_warn_activedurate`| Duration String | `1M` | Expiration time for a warn |
|`bsql_warn_autoban`| Int | `5` | Autoban player when he breaks the limit fix by this config. |
|`bsql_warn_autoban_duration`| Duration String | `50y` | Autoban duration. |

## Translation

You can find the translation file (`bansql.txt`) in `sm_translations` located into the root directory of your server.

### General Translations

|Translation key|Default|
|---------------|-------|
|`general.exception`|`Exception detected : [exception]`|
|`general.yes`|`Yes`|
|`general.no`|`No`|
|`general.playernotfound"`|`Error, player not found.`|
|`general.invalidvalue"`|`Error, Invalide value [error]", "bansql`|

### Ban Translation s

|Translation key|Default|
|---------------|-------|
|`ban.defaultReason`|`No reason was given`|
|`ban.banMessage`|`You have been banned\nReason : [reason]\nAdmin : [admin]\nUnban Date : [unbanDate]`|
|`ban.ipbanMessage`|`You're IP Address [ip] is banned\nReason : [reason]\nAdmin : [admin]\nUnban Date : [unbanDate]"`|
|`ban.disabled`|`Ban module disabled`|

### Warn Translations

|Translation key|Default|
|---------------|-------|
|`warn.autoban.banMessage`|`You've been banned because you've reached\nthe maximum warn cap`|
|`warn.disabled`|`Warn module disabled`
|`warn.broadcast.playerWarned`|`[player] has been warned by [admin]\nfor the reason\n[warn]`|

### Command Translations

|Translation key|Default|
|---------------|-------|
|`command.personnalWarn.warn`|`You've got [number] warns ([nbactive] active) :\n[warn]`|
|`command.personnalWarn.nowarn`|`You haven't got any warn.`|
|`command.playerWarned`|`[player] has been successfully warned for [reason]`|
|`command.playerBanned`|`[player] has been successfully banned for [reason] during [time].`|
|`command.getWarn`|`Warn for player [player] :\n[warns]`|
|`command.getWarn.noWarn`|`The player has no warn.`|
|`command.success`|`Success`|
|`command.error`|`Error with the command [command] : [exception]`|
